FROM registry.gitlab.com/kc-ml2-take-home-project/container_images/base-image:latest

WORKDIR /app

COPY entrypoint.sh .
RUN chmod 777 entrypoint.sh

COPY src/ .
COPY app.py .

RUN python -m spacy download en
RUN python -m spacy download de

ENTRYPOINT ["./entrypoint.sh"]