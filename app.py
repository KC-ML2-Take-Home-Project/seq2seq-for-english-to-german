import json
import sys
import os
import time

from src.util.tokenizer import Tokenizer
from src.util.data_generator import DataGenerator
from src.util.module_param import ModuleParam
from src.util.seed_initializer import SeedInitializer

from src.model.coder.encoder.encoder_one import EncoderOne
from src.model.coder.decoder.decoder_one import DecoderOne

from src.model.seq2seq.seq2seq_one_one import Seq2SeqOneOne


if __name__ == "__main__":
    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, sys.argv[1])
    assert os.path.isfile(filename) == True, "File does not exist"
    file_object = open(filename, 'r')
    layers_param = json.load(file_object)
    
    SEED = int(layers_param["SEED"])
    SRC_TYPE = layers_param["SRC_TYPE"]
    TRG_TYPE = layers_param["TRG_TYPE"]
    BATCH_SIZE = int(layers_param["BATCH_SIZE"])
    DEVICE = layers_param["DEVICE"]
    MIN_FREQ = int(layers_param["MIN_FREQ"])
    DATASET = layers_param['DATASET']
    ITERATOR = layers_param['ITERATOR']
    
    OPTIMIZER = layers_param["OPTIMIZER"]
    LOSS_FUNC = layers_param["LOSS_FUNC"]

    N_EPOCHS = int(layers_param["N_EPOCHS"])
    CLIP = int(layers_param["CLIP"])
    
    seed_initializer = SeedInitializer(SEED)
    seed_initializer.initialize()

    src_tokenizer = Tokenizer(SRC_TYPE, True)
    trg_tokenizer = Tokenizer(TRG_TYPE)

    src_tokenizer.tokenize()
    trg_tokenizer.tokenize()

    trg_field = trg_tokenizer.tokenized_field

    data_generator = DataGenerator(src_tokenizer, trg_tokenizer, DATASET, ITERATOR, BATCH_SIZE, DEVICE) # change
    data_generator.split_data()
    data_generator.build_vocab(MIN_FREQ)
    SRC_DIM, TRG_DIM = data_generator.get_dimensions_of_data()

    layers_param['SRC_DATA_DIM'] = SRC_DIM # change
    layers_param['TRG_DATA_DIM'] = TRG_DIM # change
    train_iterator, validation_iterator, test_iterator = data_generator.split_iterator()

    module_param = ModuleParam(layers_param)

    enc = EncoderOne(module_param)
    dec = DecoderOne(module_param)

    model = Seq2SeqOneOne(enc, dec, module_param)