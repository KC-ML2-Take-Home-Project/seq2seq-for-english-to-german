import os
import random
import numpy as np
import torch


class SeedInitializer:
    def __init__(self, seed_size):
        self.seed_size = seed_size

    def initialize(self):
        random.seed(self.seed_size)
        np.random.seed(self.seed_size)
        torch.manual_seed(self.seed_size)
        torch.cuda.manual_seed(self.seed_size)
        torch.backends.cudnn.deterministic = True
