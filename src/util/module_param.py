import os.path
import json


class ModuleParam:

    def __init__(self, param_in_json: dict):        
        self.__src_data_dim = param_in_json['SRC_DATA_DIM'] if 'SRC_DATA_DIM' in param_in_json else None
        self.__src_emb_dim = param_in_json['SRC_EMB_DIM'] if 'SRC_EMB_DIM' in param_in_json else None
        self.__src_dropout = param_in_json['SRC_DROPOUT'] if 'SRC_DROPOUT' in param_in_json else None
        self.__trg_data_dim = param_in_json['TRG_DATA_DIM'] if 'TRG_DATA_DIM' in param_in_json else None
        self.__trg_emb_dim = param_in_json['TRG_EMB_DIM'] if 'TRG_EMB_DIM' in param_in_json else None
        self.__trg_dropout = param_in_json['TRG_DROPOUT'] if 'TRG_DROPOUT' in param_in_json else None

        self.__hid_dim = param_in_json['HID_DIM'] if 'HID_DIM' in param_in_json else None
        self.__n_layers = param_in_json['N_LAYERS'] if 'N_LAYERS' in param_in_json else None
        self.__device = param_in_json['DEVICE'] if 'DEVICE' in param_in_json else None
        # more to come if needed 

    def get_src_data_dim(self): return self.__src_data_dim

    def get_src_emb_dim(self): return self.__src_emb_dim

    def get_src_dropout(self): return self.__src_dropout

    def get_trg_data_dim(self): return self.__trg_data_dim

    def get_trg_emb_dim(self): return self.__trg_emb_dim

    def get_trg_dropout(self): return self.__trg_dropout

    def get_hid_dim(self): return self.__hid_dim

    def get_n_layers(self): return self.__n_layers  

    def get_device(self): return self.__device
