from torchtext.data import Field
import spacy

class Tokenizer:

    def __init__(self, language_type, reverse = False):
        self.language = language_type
        self.spacy = spacy.load(self.language)
        self.is_reversed = reverse
        self.tokenized_field = None
    
    def __tokenize_spacy_text(self, text):
        if self.is_reversed:
            return [tok.text for tok in self.spacy.tokenizer(text)][::-1]
        else:
            return [tok.text for tok in self.spacy.tokenizer(text)]

    def tokenize(self):
        self.tokenized_field = Field(self.__tokenize_spacy_text,
            init_token = '<sos>',
            eos_token = '<eos>',
            lower = True)
    
    def build_vocab(self, data, min_freq):
        self.tokenized_field.build_vocab(data, min_freq=min_freq)


            