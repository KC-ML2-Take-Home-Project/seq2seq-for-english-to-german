from src.util.tokenizer import Tokenizer
from torchtext.datasets import Multi30k
from torchtext.data import BucketIterator


class DataGenerator:

    def __init__(self, input_tokenizer: Tokenizer, output_tokenizer: Tokenizer, dataset_path: str,iterator: str, batch_size: int, device_env: str):
        self.input_tokenizer = input_tokenizer
        self.output_tokenizer = output_tokenizer
        self.src = self.input_tokenizer.tokenized_field
        self.trg = self.output_tokenizer.tokenized_field
        self.batch_size = batch_size
        self.dataset = dataset_path
        self.iterator = iterator
        self.device = device_env
        self.train = None
        self.test = None
        self.validation = None
    
    def split_data(self):
        input_type = self.input_tokenizer.language
        output_type = self.output_tokenizer.language
        
        if self.dataset.lower() == 'multi30k':
            train, validation, test = Multi30k.splits(exts = ('.' + input_type, '.' + output_type),\
                fields = (self.src, self.trg))
            self.train = train
            self.validation = validation
            self.test = test
            return self.train, self.validation, self.test
        else:
            return None, None, None
        
    def build_vocab(self, min_frequency:int, data_type:str='train'):
        if data_type.lower() == 'train':
            self.input_tokenizer.build_vocab(self.train, min_frequency)
            self.output_tokenizer.build_vocab(self.train, min_frequency)
        elif data_type.lower() == 'validation':
            self.input_tokenizer.build_vocab(self.validation, min_frequency)
            self.output_tokenizer.build_vocab(self.validation, min_frequency)   
        else:
            self.input_tokenizer.build_vocab(self.test, min_frequency)
            self.output_tokenizer.build_vocab(self.test, min_frequency)      
    
    def get_dimensions_of_data(self):
        input_dim = len(self.src.vocab)
        output_dim = len(self.trg.vocab)
        return input_dim, output_dim

    def split_iterator(self):
        if self.iterator.lower() == 'bucketiterator':
            train_iterator, validation_iterator, test_iterator = BucketIterator.splits(
                (self.train, self.validation, self.test),
                batch_size = self.batch_size, device = self.device)
            return train_iterator, validation_iterator, test_iterator
        else:
            return None, None, None
