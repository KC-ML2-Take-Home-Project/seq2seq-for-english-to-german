from src.model.coder.encoder.encoder_base import Encoder
from src.util.module_param import ModuleParam
import torch.nn as nn


class EncoderOne(Encoder):
    def __init__(self, encoder_block_param: ModuleParam):
        super(EncoderOne, self).__init__(encoder_block_param)
        self.src_dropout= encoder_block_param.get_src_dropout()

        self.recurrent = nn.LSTM(self.src_emb_dim, self.hid_dim, self.n_layers, dropout=self.src_dropout)
        self.dropout = nn.Dropout(self.src_dropout)

    def forward(self, src):
        embedded = self.dropout(self.embedding(src))
        outputs, (hidden, cell) = self.recurrent(embedded)
        return hidden, cell
