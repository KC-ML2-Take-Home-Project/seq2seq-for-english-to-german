from src.util.module_param import ModuleParam
import torch.nn as nn
from abc import ABC, abstractmethod

class Encoder(nn.Module, ABC):
    def __init__(self, encoder_block_param: ModuleParam):
        super(Encoder, self).__init__()
        self.hid_dim = encoder_block_param.get_hid_dim()
        self.n_layers = encoder_block_param.get_n_layers()
        self.src_dim = encoder_block_param.get_src_data_dim()
        self.src_emb_dim = encoder_block_param.get_src_emb_dim()
        self.embedding = nn.Embedding(self.src_dim, self.src_emb_dim)

    @abstractmethod
    def forward(self, src):
        raise NotImplementedError