from src.model.coder.decoder.decoder_base import Decoder
from src.util.module_param import ModuleParam
import torch.nn as nn


class DecoderOne(Decoder):
    def __init__(self, decoder_block_param: ModuleParam):
        super(DecoderOne, self).__init__(decoder_block_param)
        self.trg_dropout= decoder_block_param.get_trg_dropout()

        self.recurrent = nn.LSTM(self.trg_emb_dim, self.hid_dim, self.n_layers, dropout=self.trg_dropout)
        self.fc_out = nn.Linear(self.hid_dim, self.trg_data_dim)
        self.dropout = nn.Dropout(self.trg_dropout)

    def forward(self, trg, hidden, cell):
        trg = trg.unsqueeze(0)
        embedded = self.dropout(self.embedding(trg))
        output, (hidden, cell) = self.recurrent(embedded, (hidden, cell))     
        prediction = self.fc_out(output.squeeze(0))
        return prediction, hidden, cell
