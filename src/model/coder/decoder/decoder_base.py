from src.util.module_param import ModuleParam
import torch.nn as nn
from abc import ABC, abstractmethod

class Decoder(nn.Module, ABC):
    def __init__(self, decoder_block_param: ModuleParam):
        super(Decoder, self).__init__()
        self.hid_dim = decoder_block_param.get_hid_dim()
        self.n_layers = decoder_block_param.get_n_layers()
        self.trg_data_dim = decoder_block_param.get_trg_data_dim()
        self.trg_emb_dim = decoder_block_param.get_trg_emb_dim()
        self.embedding = nn.Embedding(self.trg_data_dim, self.trg_emb_dim)

    @abstractmethod
    def forward(self, trg, hidden, cell):
        raise NotImplementedError