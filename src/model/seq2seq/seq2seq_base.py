import torch
import torch.nn as nn

from abc import ABC, abstractmethod

from src.model.coder.encoder.encoder_base import Encoder
from src.model.coder.decoder.decoder_base import Decoder
from src.util.module_param import ModuleParam


class Seq2Seq(nn.Module, ABC):
    def __init__(self, encoder: Encoder, decoder: Decoder, module_param: ModuleParam):
        super(Seq2Seq, self).__init__()
        self.encoder = encoder
        self.decoder = decoder

        assert encoder.hid_dim == decoder.hid_dim, \
            "Hidden dimensions of encoder and decoder must be equal!"
        assert encoder.n_layers == decoder.n_layers, \
            "Encoder and decoder must have equal number of layers!"
        
        device = module_param.get_device()
        self.set_device(device)

    def set_device(self, device):
        torch.device(device)

    @abstractmethod
    def forward(self, src, trg):
        raise NotImplementedError
    
    @abstractmethod
    def set_initial_weights(self):
        raise NotImplementedError

    @abstractmethod
    def set_optimizer(self):
        raise NotImplementedError
    
    @abstractmethod
    def set_loss(self):
        raise NotImplementedError

    @abstractmethod
    def fit(self):
        raise NotImplementedError

    @abstractmethod
    def eval(self):
        raise NotImplementedError
