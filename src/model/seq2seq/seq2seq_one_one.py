import random
import torch
import torch.nn as nn
import torch.optim as optim

from src.model.coder.encoder.encoder_base import Encoder
from src.model.coder.decoder.decoder_base import Decoder
from src.model.seq2seq.seq2seq_base import Seq2Seq

from src.util.module_param import ModuleParam


class Seq2SeqOneOne(Seq2Seq):
    def __init__(self, encoder: Encoder, decoder: Decoder, seq2seq_module_param: ModuleParam):
        super(Seq2SeqOneOne, self).__init__(encoder, decoder, seq2seq_module_param)
        
    def forward(self, src, trg, teacher_forcing_ratio = 0.5):
        batch_size = trg.shape[1]
        trg_len = trg.shape[0]
        trg_vocab_size = self.decoder.output_dim
        
        outputs = torch.zeros(trg_len, batch_size, trg_vocab_size).to(self.device)

        hidden, cell = self.encoder(src)
        
        input = trg[0,:]
        
        for t in range(1, trg_len):
            output, hidden, cell = self.decoder(input, hidden, cell)
            outputs[t] = output
            teacher_force = random.random() < teacher_forcing_ratio
            top1 = output.argmax(1) 
            input = trg[t] if teacher_force else top1
        
        return outputs
    
    def set_trg(self, trg):
        self.trg = trg

    def set_initial_weights(self):
        def init_weights(m):
            for name, param in m.named_parameters():
                nn.init.uniform_(param.data, -0.08, 0.08)
        self.apply(init_weights)
    
    def set_optimizer(self):
        self.optimizer = optim.Adam(self.parameters())
    
    def set_loss(self):
        TRG_PAD_IDX = self.trg.vocab.stoi[self.trg.pad_token]
        self.criterion = nn.CrossEntropyLoss(ignore_index = TRG_PAD_IDX)

    def fit(self, iterator):
        
        self.train()
        
        epoch_loss = 0
        
        for i, batch in enumerate(iterator):
            
            src = batch.src
            trg = batch.trg
            
            self.optimizer.zero_grad()
            
            output = self(src, trg)

            output_dim = output.shape[-1]
            
            output = output[1:].view(-1, output_dim)
            trg = trg[1:].view(-1)

            loss = self.criterion(output, trg)
            
            loss.backward()
            
            nn.utils.clip_grad_norm_(model.parameters(), self.clip)
            
            optimizer.step()
            
            epoch_loss += loss.item()
            
        return epoch_loss / len(iterator)


    def eval(self, iterator):
    
        self.eval()
        
        epoch_loss = 0
        
        with torch.no_grad():
        
            for i, batch in enumerate(iterator):

                src = batch.src
                trg = batch.trg

                output = self(src, trg, 0)

                output_dim = output.shape[-1]
                
                output = output[1:].view(-1, output_dim)
                trg = trg[1:].view(-1)
                loss = self.criterion(output, trg)
                
                epoch_loss += loss.item()
            
        return epoch_loss / len(iterator)
