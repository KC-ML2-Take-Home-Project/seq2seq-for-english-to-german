# Seq2Seq for English-to-German Translation
This is a seq-2-seq model for english-to-german translation. The project uses Python as main language, Pytorch to enable deep learning model, and is containerized using Docker.

## Pre-requisites
* Docker
* Visual Studio Code
### How to install extension
Extension for Docker and Python should be installed. [The following link](https://code.visualstudio.com/docs/editor/extension-gallery) explains the steps.
1. In the horizontal left bar, click Extension
2. In the search bar, type Python, and you will see the list of extensions.
3. Click Install of the extension placed at the top
4. Repeat for Docker

## How to Run
### In MacOS
* Make sure your Docker is running in your computer
* Open Visual Studio Code
* In the menu, click 'File' - 'New Window'
* Find the directory containing this project
* Click and open the project
* If Docker extension is installed, you will discover a green box in lower-left corner. Click, and you will see the dropdown list in the upper center of the screen.
* Click 'Open Folder in a Container'
* Click 'Dockerfile'
* If done correctly, you will see a progress bar in lower right corner; a virtual container is being created for this project.
* One the process is complete, you will see the reopening of VS code and the green box stating 'Dev Container: Existing Dockerfile'.
* In the menu, click 'Terminal' - 'New Terminal', and you will see the terminal opened at the bottom
* Type the command `python app.py`

## File Structure
* `/data`: a directory containing json files for parameters (used to initliaze `ModuleParam`)
* `/model`: a directory containing all `nn.Module`-inheriting models
    * `/decoder`: a directory containing files for Decoder classes 
        * `/decoder_base.py`: implements a class `Decoder`, an abstract base class for all Decoder-inheriting classes.
    * `/encoder`: a directory containing files for Decoder classes 
        * `/encoder_base.py`: implements a class `Encoder`, an abstract base class for all Encoder-inheriting classes. 
    * `/seq2seq`: a directory containing files for sequence-to-sequence classes  
        * `/seq2seq_base.py`: implements a class `Seq2Seq`, an abstract base class for all Seq2Seq-inheriting classes. 
* `/util`: a directory containing utility-related classes (e.g. text tokenizer)
    * `/data_generator.py`: implements class `DataGenerator`, which enables the splitting of data and iterator
    * `/module_param.py`: implements class `ModuleParam`, which operates upon parameters from the json file in the directory `/data`
    * `/seed_initializer.py`: implements class `SeedInitializer`, which calls python and pytorch modules to initialze seed size
    * `/tokenizer.py`: implements class `Tokenizer`, which enables tokenization of input/output text (uses spacy module)
* `/test`: a directory containing the test functions (currently, no test functions exist)
* `/.gitlab-ci.yml`: a CI/CD script to enable image deployment upon code push to GitLab. It uses the base CI/CD file from the project `KC-ML2-Take-Home-Project/base_ci`. 
* `app.py`: a file that triggers main function
* `Dockerfile`: a file that creates Dockerized environment of the project. It uses `registry.gitlab.com/kc-ml2-take-home-project/container_images/base-image:latest` as base image.

## Rules
* One class definition per one file
* Base class should be abstract and should be the interface for its subclass (uses abc.ABC module)
* Function & variable names must be lowecase, and should have _ between words
* Function & variable names should be named to represent its purporse (no abbreviation)

## Things To Be Improved
* No tests: there should have been tests involved to test the functionality of implemented classes. Ideal tests should basically use the implemented models to generate output and make comparison between the generated output and the pre-defined output. This implies that pre-defined dataset should be the ones used for validation and/or test. 
* Use of UML tools: A diagram should have been designed in order for developers to clearly identify classes/functions to be implemented. 
* Docker vs other virutal envioronment: Currently, it works in a Dockerized environment along with Docker extension of Visual Studio Code. Instead of using Docker, I could have used standard Python-focused virual environment and avoided the installation of addiitonal packages. In fact, other environments such as Anaconda could have provided more benefits as they are closed tied to Python.